<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="model.Book"%>

<%	
 ArrayList<Book> libros = (ArrayList<Book>)request.getAttribute("libros");
%>
<html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>JSP Page</title>
        </head>
        <body>
            <h1>Mis Libros</h1>        
            <table border="1">
                <thead>
                <th>Titulo</th>
                <th>Autor</th>
                <th>Paginas</th>    
                </thead>
                <tbody>
               <% for(Book libro : libros){%>
                <tr>
                   <td><%= libro.getTitle() %></td>
                   <td><%= libro.getAuthor() %></td>
                   <td><%= libro.getPages() %></td>
                </tr>
               <%}%>                
                </tbody>           
            </table>
                <a href="new">Volver</a>  
        </body>   
    </html>