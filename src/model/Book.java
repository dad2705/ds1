package model;

import java.util.ArrayList;

public class Book {


	public String title;
	public String author;
    public String pages;
    ArrayList<Book> biblioteca;
    
	public Book() {
		super();
	}


	public Book(String title, String author, String pages) {

		this.title = title;
		this.author = author;
		this.pages = pages;
	}





	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPages() {
		return pages;
	}

	public void setPages(String pages) {
		this.pages = pages;
	}

	@Override
	public String toString() {
		return "Book [title=" + title + ", author=" + author + ", pages=" + pages + ", biblioteca=" + biblioteca + "]";
	}
	
	
	public void getLibros(ArrayList<Book> biblioteca){
	
	}

    
}
